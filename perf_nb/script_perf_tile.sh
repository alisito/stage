#!/bin/bash
#SBATCH -N 1
#SBATCH --exclusive
#SBATCH --time=02:00:00
#SBATCH -C bora

algo=$1
nb=1
mink=240
maxk=1280
stepk=20
range="$(( mink * nb )):$(( maxk * nb )):$(( stepk * nb ))"

numactl --interleave=all /home/alisito/chameleon/build/testing/chameleon_dtesting -o $algo -b $range --mtxfmt=1 --norm Onenorm -n 20000 -l 10 >> kernels/bora_tile_nb_$algo.csv
