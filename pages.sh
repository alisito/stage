#!/bin/bash

set -x

mkdir -p public

# cd perf_300
# guix shell --pure r r-ggplot2 r-plyr r-gridextra r-scales -- Rscript kernels_N.R
# cp *.png ../public
# cd ..

cd perf_nb
guix shell --pure r r-ggplot2 r-plyr r-gridextra r-scales -- Rscript kernels_nb.R
cp *.png ../public
cd ..

cd perf_n
guix shell --pure r r-ggplot2 r-plyr r-gridextra r-scales -- Rscript kernels_N.R
cp *.png ../public
cd ..

cd perf_getrf
guix shell --pure r r-ggplot2 r-plyr r-gridextra r-scales -- Rscript kernels_N.R
cp *.png ../public
cd ..

cd perf_gemm
guix shell --pure r r-ggplot2 r-plyr r-gridextra r-scales -- Rscript kernels_N.R
cp *.png ../public
cd ..

touch README.org
guix shell --pure bash coreutils emacs emacs-org emacs-org-contrib emacs-org-ref -- emacs --batch --no-init-file --load publish.el --funcall org-publish-all
mv README.html public/index.html
