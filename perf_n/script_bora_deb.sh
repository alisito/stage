#!/bin/bash

sbatch --job-name="chameleon_gemm0" -C bora script_perf.sh gemm 720 0 0 bora 1 13 1  
sbatch --job-name="chameleon_gemm1" -C bora script_perf.sh gemm 720 0 1 bora 1 13 1
sbatch --job-name="chameleon_gemm2" -C bora script_perf.sh gemm 720 1 0 bora 1 13 1

sbatch --job-name="chameleon_trsm0" -C bora script_perf.sh trsm 720 0 0 bora 1 13 1
sbatch --job-name="chameleon_trsm1" -C bora script_perf.sh trsm 720 0 1 bora 1 13 1
sbatch --job-name="chameleon_trsm2" -C bora script_perf.sh trsm 720 1 0 bora 1 13 1

sbatch --job-name="chameleon_gesv0" -C bora script_perf.sh gesv 720 0 0 bora 1 13 1
sbatch --job-name="chameleon_gesv1" -C bora script_perf.sh gesv 720 0 1 bora 1 13 1

sbatch --job-name="chameleon_getrf0" -C bora script_perf.sh getrf 720 0 0 bora 1 13 1
sbatch --job-name="chameleon_getrf1" -C bora script_perf.sh getrf 720 0 1 bora 1 13 1

sbatch --job-name="chameleon_posv0" -C bora script_perf.sh posv 720 0 0 bora 1 13 1
sbatch --job-name="chameleon_posv1" -C bora script_perf.sh posv 720 0 1 bora 1 13 1
sbatch --job-name="chameleon_posv2" -C bora script_perf.sh posv 720 1 0 bora 1 13 1

sbatch --job-name="chameleon_potrf0" -C bora script_perf.sh potrf 720 0 0 bora 1 13 1
sbatch --job-name="chameleon_potrf1" -C bora script_perf.sh potrf 720 0 1 bora 1 13 1
sbatch --job-name="chameleon_potrf2" -C bora script_perf.sh potrf 720 1 0 bora 1 13 1

sbatch --job-name="chameleon_poinv0" -C bora script_perf.sh poinv 720 0 0 bora 1 13 1
sbatch --job-name="chameleon_poinv1" -C bora script_perf.sh poinv 720 0 1 bora 1 13 1
sbatch --job-name="chameleon_poinv2" -C bora script_perf.sh poinv 720 1 0 bora 1 13 1

sbatch --job-name="chameleon_lange0" -C bora script_perf.sh lange 720 0 0 bora 1 13 1
sbatch --job-name="chameleon_lange1" -C bora script_perf.sh lange 720 0 1 bora 1 13 1
sbatch --job-name="chameleon_lange2" -C bora script_perf.sh lange 720 1 0 bora 1 13 1

sbatch --job-name="chameleon_lansy0" -C bora script_perf.sh lansy 720 0 0 bora 1 13 1
sbatch --job-name="chameleon_lansy1" -C bora script_perf.sh lansy 720 0 1 bora 1 13 1
sbatch --job-name="chameleon_lansy2" -C bora script_perf.sh lansy 720 1 0 bora 1 13 1
