#!/bin/bash
#SBATCH -N 1
#SBATCH --exclusive
#SBATCH --time=04:00:00                                                                                                                                                   

source ~/setenv.sh 

if [ $5 = "zonda" ]
then
    export MKL_DEBUG_CPU_TYPE=5
    export MKL_ENABLE_INSTRUCTIONS=AVX512
    export MKL_NUM_THREADS=1
fi

algo=$1
nb=$2
api=$3
tile=$4
mink=$6
maxk=$7
stepk=$8
range="$(( mink * nb )):$(( maxk * nb )):$(( stepk * nb ))"

numactl --interleave=all /home/alisito/chameleon/build/testing/chameleon_dtesting -o $algo -n $range --api $3 --mtxfmt=$4 --norm Onenorm -b $nb -l 10 >> kernels/$5_$1_$3_$4.csv
