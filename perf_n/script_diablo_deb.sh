#!/bin/bash

sbatch --job-name="chameleon_gemm0" -C diablo -x diablo0[1-6] script_perf.sh gemm 300 0 0 diablo 3 30 3 
sbatch --job-name="chameleon_gemm1" -C diablo -x diablo0[1-6] script_perf.sh gemm 300 0 1 diablo 3 30 3
sbatch --job-name="chameleon_gemm2" -C diablo -x diablo0[1-6] script_perf.sh gemm 300 1 0 diablo 3 30 3

sbatch --job-name="chameleon_trsm0" -C diablo -x diablo0[1-6] script_perf.sh trsm 300 0 0 diablo 3 30 3
sbatch --job-name="chameleon_trsm1" -C diablo -x diablo0[1-6] script_perf.sh trsm 300 0 1 diablo 3 30 3
sbatch --job-name="chameleon_trsm2" -C diablo -x diablo0[1-6] script_perf.sh trsm 300 1 0 diablo 3 30 3

sbatch --job-name="chameleon_gesv0" -C diablo -x diablo0[1-6] script_perf.sh gesv 300 0 0 diablo 3 30 3
sbatch --job-name="chameleon_gesv1" -C diablo -x diablo0[1-6] script_perf.sh gesv 300 0 1 diablo 3 30 3

sbatch --job-name="chameleon_getrf0" -C diablo -x diablo0[1-6] script_perf.sh getrf 300 0 0 diablo 3 30 3
sbatch --job-name="chameleon_getrf1" -C diablo -x diablo0[1-6] script_perf.sh getrf 300 0 1 diablo 3 30 3

sbatch --job-name="chameleon_posv0" -C diablo -x diablo0[1-6] script_perf.sh posv 300 0 0 diablo 3 30 3
sbatch --job-name="chameleon_posv1" -C diablo -x diablo0[1-6] script_perf.sh posv 300 0 1 diablo 3 30 3
sbatch --job-name="chameleon_posv2" -C diablo -x diablo0[1-6] script_perf.sh posv 300 1 0 diablo 3 30 3

sbatch --job-name="chameleon_potrf0" -C diablo -x diablo0[1-6] script_perf.sh potrf 300 0 0 diablo 3 30 3
sbatch --job-name="chameleon_potrf1" -C diablo -x diablo0[1-6] script_perf.sh potrf 300 0 1 diablo 3 30 3
sbatch --job-name="chameleon_potrf2" -C diablo -x diablo0[1-6] script_perf.sh potrf 300 1 0 diablo 3 30 3

sbatch --job-name="chameleon_poinv0" -C diablo -x diablo0[1-6] script_perf.sh poinv 300 0 0 diablo 3 30 3
sbatch --job-name="chameleon_poinv1" -C diablo -x diablo0[1-6] script_perf.sh poinv 300 0 1 diablo 3 30 3
sbatch --job-name="chameleon_poinv2" -C diablo -x diablo0[1-6] script_perf.sh poinv 300 1 0 diablo 3 30 3

sbatch --job-name="chameleon_lange0" -C diablo -x diablo0[1-6] script_perf.sh lange 300 0 0 diablo 3 30 3
sbatch --job-name="chameleon_lange1" -C diablo -x diablo0[1-6] script_perf.sh lange 300 0 1 diablo 3 30 3
sbatch --job-name="chameleon_lange2" -C diablo -x diablo0[1-6] script_perf.sh lange 300 1 0 diablo 3 30 3

sbatch --job-name="chameleon_lansy0" -C diablo -x diablo0[1-6] script_perf.sh lansy 300 0 0 diablo 3 30 3
sbatch --job-name="chameleon_lansy1" -C diablo -x diablo0[1-6] script_perf.sh lansy 300 0 1 diablo 3 30 3
sbatch --job-name="chameleon_lansy2" -C diablo -x diablo0[1-6] script_perf.sh lansy 300 1 0 diablo 3 30 3
