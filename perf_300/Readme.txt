sbatch --job-name="name" -C machine script_perf.sh args

args:
$1 = name of the algo
$2 = size of the tile
$3 = api (desc=0, std=1)
$4 = tile (no=0, yes=1)
$5 = name of the machine

name of the file : 
machine_algo_api_nb_tile.csv